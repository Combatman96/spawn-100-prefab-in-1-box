
using System;
using UnityEngine;

public class CircleController : MonoBehaviour
{
    private bool isOverlapping = false;

    public LayerMask canNotOverLap;

    private void FixedUpdate()
    {
        var collier = Physics2D.OverlapCircle(this.transform.position, 0.5f, canNotOverLap);
        if (collier != null)
            isOverlapping = true;
    }

    public bool IsOverlapping()
    {
        Debug.Log(isOverlapping);
        return isOverlapping;
    }
}
