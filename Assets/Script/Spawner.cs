using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

public class Spawner : MonoBehaviour
{
    public GameObject prefabCircle;

    [Header("Parameters")] 
    public int numOfPrefab = 0;

    public float Xlength = 19f;
    public float Ylength = 19f;

    private GameObject circle;

    [SerializeField] private int count = 0;

    private bool isOverLapping = false;

    public LayerMask overLappingLayer;

    public BoxCollider2D _box;
    private float startX;
    private float startY;
  
    
    
    // Start is called before the first frame update
    void Start()
    {
        var size = _box.size;
        startX = size.x * 0.5f - 0.5f;
        startY = size.y * 0.5f - 0.5f;
        
        //Spawn();
    
        //Spawn2();
        
        Spawn3();
    }

    

    void Spawn()
    {
        //Out of mem :(((
        count = 0;

        Vector3 firstSpawnPos = new Vector3(Random.Range(1f, Xlength),Random.Range(1f, Ylength),0);
        
        List<Vector3> currentSpawnPos = new List<Vector3>();
        List<Vector3> canSpawnPos = new List<Vector3>();
        
        currentSpawnPos.Add(firstSpawnPos);
        canSpawnPos.Add(firstSpawnPos);
        
        while (count < numOfPrefab)
        {
            //A box 20x20
            float coordX = Random.Range(1f, Xlength);
            float coordY = Random.Range(1f, Ylength);
            
            Vector3 newSpawnPos = new Vector3(coordX, coordY , 0);

            foreach (var pos in currentSpawnPos)
            {
                if (Vector3.Distance(pos, newSpawnPos) < 0.5f)
                {
                    break;
                }
                else
                {
                    canSpawnPos.Add(newSpawnPos);
                }
            }
        }

        foreach (var pos in canSpawnPos)
        {
            Instantiate(prefabCircle, pos, quaternion.identity);
        }
    }

    void Spawn2()
    {
        float X = 4 * Xlength;
        float Y = 4 * Ylength;

        for (int i = 0; i < X; i += 4)
        {
            for (int j = 0; j < Y; j +=4)
            {
                float coordX = i + Random.Range(0f, 3f);
                float coordY = j + Random.Range(0f, 3f);

                 circle = Instantiate(prefabCircle, new Vector3(coordX, coordY, 0f), Quaternion.identity);
                 count++;
            }
        }
    }


    private void FixedUpdate()
    {
        
    }


    void Spawn3()
    {
        while (count < numOfPrefab)
        {
            float coordX = Random.Range(startX * -1, startX);
            float coordY = Random.Range(startY * -1, startY);
            
            transform.position = new Vector3(coordX, coordY,0f);
            isOverLapping = Physics2D.OverlapCircle(this.transform.position, 0.5f, overLappingLayer);
            Debug.Log(isOverLapping);
            
            if (isOverLapping == true)
            {
                continue;
            }
            else
            {
                Instantiate(prefabCircle, transform.position, quaternion.identity);
                count++;
                continue;
            }
        }
    }
    
}
